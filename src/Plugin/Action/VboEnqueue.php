<?php

namespace Drupal\vbo_queue\Plugin\Action;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a an Add an item to the queue action.
 *
 * @Action(
 *   id = "vbo_queue_enqueue",
 *   label = @Translation("Add an item to the queue"),
 *   category = @Translation("VBO enqueue"),
 *   deriver = "Drupal\vbo_queue\Plugin\Derivative\VboQueueDeriver"
 * )
 */
class VboEnqueue extends ViewsBulkOperationsActionBase implements ContainerFactoryPluginInterface {

  /**
   * Manage drupal queue workers.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * Load the queue where the items will be insert.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Do replacements in queue payload.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->setQueueWorkerPlugin($container->get('plugin.manager.queue_worker'));
    $instance->setQueueFactory($container->get('queue'));
    $instance->setToken($container->get('token'));
    return $instance;
  }

  /**
   * Set the queue worker plugin.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $queue_worker_manager
   *   Queue manager.
   */
  public function setQueueWorkerPlugin(PluginManagerInterface $queue_worker_manager) {
    $this->queueWorkerManager = $queue_worker_manager;
  }

  /**
   * Set the queue factory.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue manager.
   */
  public function setQueueFactory(QueueFactory $queueFactory) {
    $this->queueFactory = $queueFactory;
  }

  /**
   * Set the token.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   Token service.
   */
  public function setToken(Token $token) {
    $this->token = $token;
  }

  /**
   * Get the list of queues available so a queue can be chosen in configuration.
   *
   * @return array
   *   Key-value with queue_id -> title.
   */
  protected function getQueueList() {
    $queue_list = [];
    foreach ($this->queueWorkerManager->getDefinitions() as $definition) {
      $queue_list[$definition['id']] = $definition['title'];
    }
    return $queue_list;
  }

  /**
   * Build queue item info.
   *
   * @param object $object
   *   Object that is currently processed.
   *
   * @return array
   *   Data.
   */
  protected function buildQueueItemInfo($object) {
    $plugin_definition = $this->getPluginDefinition();
    $data_replaced_with_tokens = $this->token->replace($this->configuration['data'], [$plugin_definition['type'] => $object]);
    return Yaml::decode($data_replaced_with_tokens);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['queue_name' => '', 'data' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreConfigurationForm(array $form, array $values, FormStateInterface $form_state) {
    $plugin_definition = $this->getPluginDefinition();
    $form['queue_name'] = [
      '#title' => $this->t('Queue name'),
      '#type'  => 'select',
      '#default_value' => !empty($values['queue_name']) ? $values['queue_name'] : '',
      '#options' => [
        '' => $this->t('Select'),
      ] + $this->getQueueList(),
      '#required' => TRUE,
    ];

    $form['data'] = [
      '#title' => $this->t('Queue data'),
      '#type' => 'textarea',
      '#default_value' => !empty($values['data']) ? $values['data'] : NULL,
      '#description' => $this->t('YAML to set the data you want send to the queue'),
      '#element_validate' => [static::class, 'validateYamlData'],
      '#required' => TRUE,
    ];

    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$plugin_definition['type']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function validateYamlData(&$element, FormStateInterface $form_state, &$complete_form) {
    try {
      Yaml::decode($element['value']);
    }
    catch (InvalidDataTypeException $e) {
      $form_state->setError($element, t('Invalid yaml data provided.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = $account->hasPermission('views bulk operations enqueue') ? AccessResult::allowed() : AccessResult::forbidden();
    if ($access->isAllowed() === FALSE) {
      $this->messenger()->addWarning($this->t("You don't have enough permissions to enqueue items."));
    }

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * Enqueu the item.
   *
   * @param mixed $queue_item
   *   Item that will be queued.
   */
  protected function doEnqueue($queue_item) {
    $queue_name = $this->configuration['queue_name'];
    if ($this->queueWorkerManager->hasDefinition($queue_name)) {
      $queue = $this->queueFactory->get($queue_name);
      $queue->createItem($queue_item);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute($object = NULL) {
    $queue_item = $this->buildQueueItemInfo($object);
    $this->doEnqueue($queue_item);
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $objects) {
    if (!$this->queueWorkerManager->hasDefinition($this->configuration['queue_name'])) {
      $this->messenger()->addWarning(sprintf('Queue with name %s does not exists.', $this->configuration['queue_name']));
      return [];
    }

    $results = parent::executeMultiple($objects);
    $ids = [];
    foreach ($objects as $object) {
      if ($object instanceof EntityInterface) {
        $ids[] = $object->id();
      }
    }

    if (count($ids) > 0) {
      $message = $this->formatPlural(
        count($ids),
        'The :entity_type with id :ids has been enqueued, it will be processed in the next cron run.',
        'The following :entity_type entities has been enqueued: :ids. They will be proccesed in the next cron run.',
        [
          ':entity_type' => $this->getPluginDefinition()['type'],
          ':ids' => implode(', ', $ids),
        ]
      );
      $this->messenger()->addStatus($message);
    }
    return $results;
  }

}
