<?php

namespace Drupal\vbo_queue_test\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Queue worker to be selected in the test vbo queue view.
 *
 * @QueueWorker(
 *   id = "vbo_queue_test",
 *   title = @Translation("Vbo queue test"),
 *   cron = {"time" = 60}
 * )
 */
class VboQueueTest extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
  }

}
