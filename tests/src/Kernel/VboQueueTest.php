<?php

namespace Drupal\vbo_queue\Tests\Kernel;

use Drupal\Core\Messenger\Messenger;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\views_bulk_operations\Kernel\ViewsBulkOperationsKernelTestBase;

/**
 * Test that items can be enqueued through vbo enqueue.
 *
 * @group vbo_queue
 */
class VboQueueTest extends ViewsBulkOperationsKernelTestBase {

  use UserCreationTrait;

  /**
   * Set default theme to stable.
   *
   * @var string
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'field',
    'content_translation',
    'language',
    'text',
    'action',
    'system',
    'node',
    'views',
    'views_bulk_operations',
    'views_bulk_operations_test',
    'vbo_queue',
    'vbo_queue_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig('vbo_queue');
    $this->installConfig('vbo_queue_test');
    $this->createTestNodes([
      'page' => [
        'count' => 1,
      ],
    ]);
  }

  /**
   * Test items can be enqueued.
   */
  public function testEnqueue() {
    $this->setUpCurrentUser([], ['views bulk operations enqueue']);
    $results = $this->executeAction([
      'view_id' => 'vbo_queue_test',
      'action_id' => 'vbo_queue_enqueue:node',
      'preconfiguration' => [
        'queue_name' => 'vbo_queue_test',
        'data' => "key: value\r\nnode_title: [node:title]",
      ],
    ]);

    $first_node = reset($this->testNodesData);
    $first_node_title = reset($first_node);

    $this->assertEquals('Processed 1 of 1 entities.', $results['messages'][0]);
    $queue = \Drupal::queue('vbo_queue_test');
    $this->assertEquals(1, $queue->numberOfItems());
    $item = $queue->claimItem();
    $item_data = $item->data;
    $this->assertIsArray($item_data);
    $this->assertArrayHasKey('key', $item_data);
    $this->assertArrayHasKey('node_title', $item_data);
    $this->assertEquals('value', $item_data['key']);
    $this->assertEquals($first_node_title, $item_data['node_title']);
  }

  /**
   * Test nothing is enqueued when the queue does not exists.
   */
  public function testEnqueueFail() {
    $this->executeAction([
      'view_id' => 'vbo_queue_test',
      'action_id' => 'vbo_queue_enqueue:node',
      'preconfiguration' => [
        'queue_name' => 'vbo_queue_not_found',
        'data' => "key: value\r\nnode_title: [node:title]",
      ],
    ]);

    $this->assertTrue(in_array('Queue with name vbo_queue_not_found does not exists.', \Drupal::messenger()->messagesByType(Messenger::TYPE_WARNING)));
    $queue = \Drupal::queue('vbo_queue_test');
    $this->assertEquals(0, $queue->numberOfItems());
  }

  /**
   * Test access is denied when an unprivileged used tries to enqueue.
   */
  public function testEnqueueInvalidUser() {
    $this->setUpCurrentUser();
    $results = $this->executeAction([
      'view_id' => 'vbo_queue_test',
      'action_id' => 'vbo_queue_enqueue:node',
      'preconfiguration' => [
        'queue_name' => 'vbo_queue_test',
        'data' => "key: value\r\nnode_title: [node:title]",
      ],
    ]);

    $this->assertEquals(1, $results['operations']['Access denied']);
    $queue = \Drupal::queue('vbo_queue_test');
    $this->assertEquals(0, $queue->numberOfItems());
  }

}
